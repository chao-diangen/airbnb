/**
 * @Author       : Leo chaodiangen@163.com
 * @Date         : 2024-04-26 10:42:54
 * @LastEditors  : Leo chaodiangen@163.com
 * @LastEditTime : 2024-04-26 10:43:01
 * @FilePath     : \react-demo\cdg_airbnb\src\views\detail\c-cpns\detail-infos\style.js
 * @Description  : 
 * @
 * @Copyright (c) 2024 by ${git_name_email}, All Rights Reserved. 
*/

import styled from "styled-components";

export const InfosWrapper = styled.div``