/*
 * @Author       : Leo chaodiangen@163.com
 * @Date         : 2024-04-26 10:42:47
 * @LastEditors  : Leo chaodiangen@163.com
 * @LastEditTime : 2024-04-28 13:54:19
 * @FilePath     : \airbnb\src\views\detail\c-cpns\detail-infos\index.jsx
 * @Description  : 
 * 
 * Copyright (c) 2024 by ${git_name_email}, All Rights Reserved. 
 */

import React, { memo } from 'react'
import {InfosWrapper} from './style'

const Infos = memo((props) => {
  return (
    <InfosWrapper>Info</InfosWrapper>
  )
})

export default Infos