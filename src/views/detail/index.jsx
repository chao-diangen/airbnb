import React, { memo, useEffect } from 'react'
import { DetailWrapper } from './style'
import Infos from './c-cpns/detail-infos'
import Picture from './c-cpns/detail-pictures'
import { useDispatch } from 'react-redux'
import { changeHeaderConfigAction } from '@/store/modules/main'

const Detail = memo(() => {
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(changeHeaderConfigAction({ isFixed: false, topAlpha: false }))
  }, [dispatch])
  return (
    <DetailWrapper>
      <Picture />
      <Infos />
    </DetailWrapper>
  )
})

export default Detail
