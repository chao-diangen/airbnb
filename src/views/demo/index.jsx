/*
 * @Author       : Leo chaodiangen@163.com
 * @Date         : 2024-04-25 16:13:51
 * @LastEditors  : Leo chaodiangen@163.com
 * @LastEditTime : 2024-04-25 17:32:10
 * @FilePath     : \react-demo\cdg_airbnb\src\views\demo\index.jsx
 * @Description  :
 *
 * Copyright (c) 2024 by ${git_name_email}, All Rights Reserved.
 */
import React, { memo, useState } from 'react'
import Indicator from '@/base-ui/indicator'
import { DemoWrapper } from './style'

const index = memo(() => {
  const names = [
    '123',
    '456',
    '789',
    '1111',
    '2222',
    '3333',
    '4444',
    '5555',
    '6666',
    '7777',
  ]
  const [selectIndex, setSelectIndex] = useState(0)

  function toggleClickHandle(isNext) {
    let newIndex = isNext ? selectIndex + 1 : selectIndex - 1
    if (newIndex < 0) newIndex = names.length - 1
    if (newIndex > names.length - 1) newIndex = 0
    setSelectIndex(newIndex)
  }
  return (
    <DemoWrapper>
      <div className="control">
        <button onClick={(e) => toggleClickHandle(false)}>上一个</button>
        <button onClick={(e) => toggleClickHandle(true)}>下一个</button>
      </div>
      <div className="list">
        <Indicator selectIndex={selectIndex}>
          {names.map((item) => {
            return <button key={item}>{item}</button>
          })}
        </Indicator>
      </div>
    </DemoWrapper>
  )
})

export default index
