/*
 * @Author       : Leo chaodiangen@163.com
 * @Date         : 2024-04-11 17:51:08
 * @LastEditors  : Leo chaodiangen@163.com
 * @LastEditTime : 2024-04-25 10:59:04
 * @FilePath     : \react-demo\cdg_airbnb\src\views\entire\index.jsx
 * @Description  : 
 * 
 * Copyright (c) 2024 by ${git_name_email}, All Rights Reserved. 
 */

import React, { memo, useEffect } from 'react'
import { EntireWrapper } from './style'
import EntireFilter from './c-cpns/entire-filter'
import EntirePagination from './c-cpns/entire-pagination'
import EntireRooms from './c-cpns/entire-rooms'
import { useDispatch } from 'react-redux'
import { fetchRoomListAction } from '@/store/modules/entire/actionCreators'
import { changeHeaderConfigAction } from '@/store/modules/main'

const index = memo(() => {
  const dispatch =  useDispatch()
  useEffect(() => {
    dispatch(fetchRoomListAction())
    dispatch(changeHeaderConfigAction({isFixed:true,topAlpha:false}))
  },[dispatch])
  return (
    <EntireWrapper>
      <EntireFilter />
      <EntireRooms />
      <EntirePagination />
    </EntireWrapper>
  )
})

export default index