import styled from 'styled-components'

export const RoomsWrapper = styled.div`
  padding: 30px 20px;
  position: relative;
  margin-top: 128PX;
  .list {
    display: flex;
    flex-wrap: wrap;
  }
  .title{
    font-size: 22px;
    color: #222;
    font-weight: 700;
    margin: 0 0 10px 10px;
  }
  >.cover{
    position: absolute;
    left: 0;
    top: 0;
    right: 0;
    bottom: 0;
    background-color: rgba(255,255,255,.8);
    height: 100%;
    width: 100%;
  }
`
