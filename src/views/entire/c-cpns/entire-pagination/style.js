import styled from 'styled-components'

export const PaginationWrapper = styled.div`
  display: flex;
  justify-content: center;
  .info {
    display: flex;
    flex-direction: column;
    align-items: center;
    .MuiPaginationItem-page{
      margin: 0 8px;
      &:hover{
        text-decoration: underline;
      }
    }
    .MuiPaginationItem-icon{
      font-size: 25px;
    }
    .MuiPaginationItem-page.Mui-selected{
      color: #fff;
      background-color: #000;
    }
    .desc {
      font-size: 16px;
      margin-top: 10px;
      color: #222;
    }
  }
`
