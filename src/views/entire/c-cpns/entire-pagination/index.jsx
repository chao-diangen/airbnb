import React, { memo } from 'react'
import { PaginationWrapper } from './style'
import Pagination from '@mui/material/Pagination'
import { shallowEqual, useDispatch, useSelector } from 'react-redux'
import {
  fetchRoomListAction,
} from '@/store/modules/entire/actionCreators'

const EntirePagination = memo((props) => {
  const { totalCount, currentPage, roomList } = useSelector((state) => ({
    totalCount: state.entire.totalCount,
    currentPage: state.entire.currentPage,
    roomList: state.entire.roomList,
  }), shallowEqual)

  const startCount = currentPage * 20 + 1
  const endCount = (currentPage + 1) * 20
  const totalPage = Math.ceil(totalCount / 20)

  // 事件处理
  const dispatch = useDispatch()
  const pageChangeClickHandle = (event, value) => {
    // 回到顶部
    window.scroll(0, 0)
    // 增加蒙版，数据请求结束蒙版消失

    // dispatch(changeCurrentPageAction(value - 1))
    dispatch(fetchRoomListAction(value - 1))
  }
  return (
    <PaginationWrapper>
      {!!roomList.length && (
        <div className="info">
          <Pagination count={totalPage} onChange={pageChangeClickHandle} />
          {/* 
              currentPage 0  0-20
              currentPage 1  21-40
          */}
          <div className="desc">
            第{startCount}-{endCount}个房源，共超过{totalCount}个
          </div>
        </div>
      )}
    </PaginationWrapper>
  )
})

export default EntirePagination
