/*
 * @Author       : Leo chaodiangen@163.com
 * @Date         : 2024-04-11 17:51:02
 * @LastEditors: chao.diangen
 * @LastEditTime: 2024-04-13 19:53:27
 * @FilePath     : \react-demo\cdg_airbnb\src\views\home\index.jsx
 * @Description  :
 *
 * Copyright (c) 2024 by ${git_name_email}, All Rights Reserved.
 */
import React, { memo, useEffect } from 'react'
import { shallowEqual, useDispatch, useSelector } from 'react-redux'

import { HomeWrapper } from './style'
import { fetchHomeDataAction } from '@/store/modules/home'

import HomeBanner from './c-cpns/home-banner'
import HomeSectionV1 from './c-cpns/home-section-v1'
import HomeSectionV2 from './c-cpns/home-section-v2'
import HomeSectionV3 from './c-cpns/home-section-v3'
import HomeLongFor from './c-cpns/home-longfor'
import { isEmptyObject } from '@/utils'
import { changeHeaderConfigAction } from '@/store/modules/main'

const index = memo(() => {
  // 当代码过多的时候 进行分类

  /** 从redux中获取数据 */
  const {
    goodPriceInfo,
    highScoreInfo,
    discountInfo,
    recommendInfo,
    longForInfo,
    plusInfo
  } = useSelector(
    (state) => ({
      goodPriceInfo: state.home.goodPriceInfo,
      highScoreInfo: state.home.highScoreInfo,
      discountInfo: state.home.discountInfo,
      recommendInfo: state.home.recommendInfo,
      longForInfo: state.home.longForInfo,
      plusInfo: state.home.plusInfo,
    }),
    shallowEqual
  )

  //  派发异步事件发起网络请求
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(fetchHomeDataAction())
    dispatch(changeHeaderConfigAction({ isFixed: true, topAlpha: true }))
  }, [dispatch])

  return (
    <HomeWrapper>
      {/* banner */}
      <HomeBanner />
      <div className="content">
        {/* 热门目的地 */}
        {isEmptyObject(discountInfo) && (
          <HomeSectionV2 infoData={discountInfo} />
        )}
        {/* 推荐 */}
        {isEmptyObject(recommendInfo) && (
          <HomeSectionV2 infoData={recommendInfo} />
        )}

        {/* 可能想去 */}
        {isEmptyObject(longForInfo) && <HomeLongFor infoData={longForInfo} />}

        {/* 高性价比 */}
        {isEmptyObject(goodPriceInfo) && (
          <HomeSectionV1 infoData={goodPriceInfo} />
        )}

        {/* 好评 */}
        {isEmptyObject(highScoreInfo) && (
          <HomeSectionV1 infoData={highScoreInfo} />
        )}
        {/* plus */}
        {isEmptyObject(plusInfo) && <HomeSectionV3 infoData={plusInfo} />}
      </div>
    </HomeWrapper>
  )
})

export default index
