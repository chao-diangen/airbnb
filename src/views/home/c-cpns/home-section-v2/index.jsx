/*
 * @Author       : Leo chaodiangen@163.com
 * @Date         : 2024-04-22 14:30:48
 * @LastEditors  : Leo chaodiangen@163.com
 * @LastEditTime : 2024-04-22 14:33:00
 * @FilePath     : \react-demo\cdg_airbnb\src\views\home\c-cpns\home-section-v2\index.jsx
 * @Description  : 
 * 
 * Copyright (c) 2024 by ${git_name_email}, All Rights Reserved. 
 */
import PropTypes from 'prop-types'
import React, { memo, useCallback, useState } from 'react'
import { SectionWrapperV2 } from './style'
import SectionTabs from '@/components/section-tabs'
import SectionHeader from '@/components/section-header'
import SectionRooms from '@/components/section-rooms'
import SectionFooter from '@/components/section-footer'

const HomeSectionV2 = memo((props) => {
  const { infoData } = props

  const initialName = Object.keys(infoData.dest_list)[0]
  // useState 只能第一次使用，后面再次使用无效
  const [name, setName] = useState(initialName);

  // 处理数据 tabNames
  const tabNames = infoData.dest_address?.map(item => {
    return item.name
  })
  const tabClickHandle = useCallback(function (index, name) {
    setName(name)
  }, [])
  return (
    <SectionWrapperV2>
      <div className="discount">
        <SectionHeader title={infoData.title} subtitle={infoData.subtitle} />
        <SectionTabs tabNames={tabNames} tabClick={tabClickHandle} />
        <SectionRooms roomList={infoData.dest_list?.[name]} itemWidth="33.3%" />
        <SectionFooter name={infoData.name} />
      </div>
    </SectionWrapperV2>
  )
})

HomeSectionV2.propTypes = {
  infoData: PropTypes.object
}

export default HomeSectionV2