/*
 * @Author       : Leo chaodiangen@163.com
 * @Date         : 2024-04-24 11:23:52
 * @LastEditors  : Leo chaodiangen@163.com
 * @LastEditTime : 2024-04-24 13:56:40
 * @FilePath     : \react-demo\cdg_airbnb\src\views\home\c-cpns\home-section-v3\index.jsx
 * @Description  :
 *
 * Copyright (c) 2024 by ${git_name_email}, All Rights Reserved.
 */

import PropTypes from 'prop-types'
import React, { memo } from 'react'
import { SectionV3Wrapper } from './style'
import SectionHeader from '@/components/section-header'
import RoomItem from '@/components/room-item'
import ScrollView from '@/base-ui/scroll-view'
import SectionFooter from '@/components/section-footer'

const HomeSectionV3 = memo((props) => {
  const { infoData } = props
  return (
    <SectionV3Wrapper>
      <SectionHeader title={infoData.title} subtitle={infoData.subtitle} />
      <div className="room-list">
        <ScrollView>
          {infoData.list.map((item, index) => {
            return <RoomItem itemData={item} key={item.id} itemWidth="20%" />
          })}
        </ScrollView>
      </div>
      <SectionFooter name="plus" />
    </SectionV3Wrapper>
  )
})

HomeSectionV3.propTypes = {
  infoData: PropTypes.object,
}

export default HomeSectionV3
