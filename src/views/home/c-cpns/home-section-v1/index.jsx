/*
 * @Author       : Leo chaodiangen@163.com
 * @Date         : 2024-04-22 14:30:48
 * @LastEditors  : Leo chaodiangen@163.com
 * @LastEditTime : 2024-04-22 14:36:03
 * @FilePath     : \react-demo\cdg_airbnb\src\views\home\c-cpns\home-section-v1\index.jsx
 * @Description  : 
 * 
 * Copyright (c) 2024 by ${git_name_email}, All Rights Reserved. 
 */
import PropTypes from 'prop-types'
import React, { memo } from 'react'

import { SectionWrapperV1 } from './style'
import SectionHeader from '@/components/section-header'
import SectionRooms from '@/components/section-rooms'
import SectionFooter from '@/components/section-footer'

const HomeSectionV1 = memo((props) => {
  const { infoData } = props
  return (
    <SectionWrapperV1>
      <SectionHeader title={infoData.title} subtitle={infoData.subtitle} />
      <SectionRooms roomList={infoData.list} />
      <SectionFooter name={infoData.name} />
    </SectionWrapperV1>
  )
})

HomeSectionV1.propTypes = {
  infoData:PropTypes.object
}

export default HomeSectionV1