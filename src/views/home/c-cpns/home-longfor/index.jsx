/*
 * @Author       : Leo chaodiangen@163.com
 * @Date         : 2024-04-24 10:27:16
 * @LastEditors  : Leo chaodiangen@163.com
 * @LastEditTime : 2024-04-24 11:14:50
 * @FilePath     : \react-demo\cdg_airbnb\src\views\home\c-cpns\home-longfor\index.jsx
 * @Description  :
 *
 * Copyright (c) 2024 by ${git_name_email}, All Rights Reserved.
 */
import PropTypes from 'prop-types'
import React, { memo } from 'react'
import { LongForWrapper } from './style'
import SectionHeader from '@/components/section-header'
import LongForItem from '@/components/longfor-item'
import ScrollView from '@/base-ui/scroll-view'

const HomeLongFor = memo((props) => {
  const { infoData } = props
  return (
    <LongForWrapper>
      <SectionHeader title={infoData.title} subtitle={infoData.subtitle} />
      <div className="longfor-list">
        <ScrollView>
          {infoData.list.map((item, index) => {
            return <LongForItem itemData={item} key={index}></LongForItem>
          })}
        </ScrollView>
      </div>
    </LongForWrapper>
  )
})

HomeLongFor.propTypes = {
  infoData: PropTypes.object,
}

export default HomeLongFor
