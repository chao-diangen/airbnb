/*
 * @Author       : Leo chaodiangen@163.com
 * @Date         : 2024-04-24 10:38:11
 * @LastEditors  : Leo chaodiangen@163.com
 * @LastEditTime : 2024-04-24 10:56:44
 * @FilePath     : \react-demo\cdg_airbnb\src\components\longfor-item\index.jsx
 * @Description  :
 *
 * Copyright (c) 2024 by ${git_name_email}, All Rights Reserved.
 */

import PropTypes from 'prop-types'
import React, { memo } from 'react'
import { ItemWrapper } from './style'

const LongForItem = memo((props) => {
  const { itemData } = props
  return (
    <ItemWrapper>
      <div className='inner'>
        <div className='item-info'>
          <img className='cover' src={itemData.picture_url} alt="" />
          <div className='bg-cover'></div>
          <div className='info'>
            <div className='city'>{itemData.city}</div>
            <div className='price'>均价 {itemData.price}</div>
          </div>
        </div>
      </div>
    </ItemWrapper>
  )
})

LongForItem.propTypes = {
  itemData: PropTypes.object,
}

export default LongForItem
