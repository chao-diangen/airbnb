import styled from "styled-components";

export const FooterWrapper = styled.div`
  margin-top: 10px;
  display: flex;
  .info{
    cursor: pointer;
    display: flex;
    align-items: center;
    font-size: 17px;
    color: ${(prop) => prop.color};
    font-weight: bold;
    &:hover{
      text-decoration: underline;
    }
    .text{
      margin-right: 6px;
    }
  }
`