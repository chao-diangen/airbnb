import PropTypes from 'prop-types'
import React, { memo } from 'react'
import IconMoreArrow from '@/assets/svg/icon-more-arrow'
import { FooterWrapper } from './style'
import { useNavigate } from 'react-router-dom'

const SectionFooter = memo((props) => {
  const { name } = props
  // 根据name 判断显示
  let showMessage = '显示全部'
  if (name) {
    showMessage = `显示更多${name}房源`
  }
  // 事件处理 跳转详情
  const navigate =  useNavigate()
  function moreClickHandle () {
    navigate('/entire')
  }
  return (
    <FooterWrapper color={name ? "#00848A": "#000"}>
      <div className='info' onClick={moreClickHandle}>
        <span className='text'>{showMessage}</span>
        <IconMoreArrow></IconMoreArrow>
      </div>
    </FooterWrapper>
  )
})

SectionFooter.propTypes = {
  name: PropTypes.string
}

export default SectionFooter