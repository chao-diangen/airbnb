/*
 * @Author       : Leo chaodiangen@163.com
 * @Date         : 2024-04-19 15:42:56
 * @LastEditors  : Leo chaodiangen@163.com
 * @LastEditTime : 2024-04-26 16:52:17
 * @FilePath     : \react-demo\cdg_airbnb\src\components\room-item\index.jsx
 * @Description  :
 *
 * Copyright (c) 2024 by ${git_name_email}, All Rights Reserved.
 */
import PropTypes from 'prop-types'
import React, { memo, useRef, useState } from 'react'
import classNames from 'classnames'

import { ItemWrapper } from './style'
import { Rating } from '@mui/material'
import { Carousel } from 'antd'
import IconArrowLeft from '@/assets/svg/icon-arrow-left'
import IconArrowRight from '@/assets/svg/icon-arrow-right'
import Indicator from '@/base-ui/indicator'

const RoomItem = memo((props) => {
  const { itemData, itemWidth = '25%', itemClick } = props
  const [selectIndex, setSelectIndex] = useState(0)
  const sliderRef = useRef()

  // 事件处理逻辑
  function controlClickHandle (isNext = true,event) {
    // 切换上下面板
    isNext ? sliderRef.current.next() : sliderRef.current.prev()
    // 阻止冒泡
    // 计算当前索引
    let newIndex = isNext ? selectIndex + 1 : selectIndex - 1
    const length = itemData.picture_urls.length
    if (newIndex < 0) newIndex = length - 1
    if (newIndex > length - 1) newIndex = 0
    setSelectIndex(newIndex)
    // 阻止事件冒泡
    event.stopPropagation()
  }
  function itemClickHandle (item) {
    if(itemClick) itemClick(item)

  }
  // 子元素赋值
  const pictureElement = (
    <div className="cover">
      <img src={itemData.picture_url} alt="" />
    </div>
  )
  const sliderElement = (
    <div className="slider">
      <div className="control">
        <div
          className="btn left"
          onClick={(e) => controlClickHandle(false,e)}
        >
          <IconArrowLeft width="30" height="30" />
        </div>
        <div
          className="btn right"
          onClick={(e) => controlClickHandle(true,e)}
        >
          <IconArrowRight width="30" height="30" />
        </div>
      </div>
      <div className="indicator">
        <Indicator selectIndex={selectIndex}>
          {itemData?.picture_urls?.map((item, index) => {
            return (
              <div className="item" key={index}>
                <span
                  className={classNames('dot', {
                    active: selectIndex === index,
                  })}
                ></span>
              </div>
            )
          })}
        </Indicator>
      </div>
      <Carousel dots={false} ref={sliderRef}>
        {itemData?.picture_urls?.map((item) => {
          return (
            <div className="cover" key={item}>
              <img src={item} alt="" />
            </div>
          )
        })}
      </Carousel>
    </div>
  )
  return (
    <ItemWrapper
      verifyColor={itemData?.verify_info?.text_color || '#39576a'}
      itemWidth={itemWidth}
      onClick={e => itemClickHandle(itemData)}
    >
      <div className="inner">
        {
          !itemData.picture_urls ? pictureElement : sliderElement
        }
        <div className="desc">{itemData.verify_info.messages.join(' · ')}</div>
        <div className="name">{itemData.name}</div>
        <div className="price">￥{itemData.price}/晚</div>
        <div className="bottom">
          <Rating
            sx={{ fontSize: '12px', color: '#008489', marginRight: '-1px' }}
            readOnly
            defaultValue={itemData?.star_rating ?? 5}
            precision={0.1}
          />
          <span className="count">{itemData?.reviews_count}</span>
          {itemData?.bottom_info && (
            <span className="extra"> · {itemData.bottom_info?.content}</span>
          )}
        </div>
      </div>
    </ItemWrapper >
  )
})

RoomItem.propTypes = {
  itemData: PropTypes.object,
}

export default RoomItem
