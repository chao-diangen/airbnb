/**
 * @Author       : Leo chaodiangen@163.com
 * @Date         : 2024-04-19 15:15:24
 * @LastEditors  : Leo chaodiangen@163.com
 * @LastEditTime : 2024-04-19 15:25:58
 * @FilePath     : \react-demo\cdg_airbnb\src\components\section-header\style.js
 * @Description  :
 * @
 * @Copyright (c) 2024 by ${git_name_email}, All Rights Reserved.
 */

import styled from 'styled-components'

export const HeaderWrapper = styled.div`
  color: #222;

  .title {
    font-size: 22px;
    font-weight: 700;
    margin-bottom: 16px;
  }

  .subtitle {
    font-size: 16px;
    margin-bottom: 20px;
  }
`
