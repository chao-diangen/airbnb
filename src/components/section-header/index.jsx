/*
 * @Author       : Leo chaodiangen@163.com
 * @Date         : 2024-04-19 15:15:17
 * @LastEditors  : Leo chaodiangen@163.com
 * @LastEditTime : 2024-04-22 14:27:00
 * @FilePath     : \react-demo\cdg_airbnb\src\components\section-header\index.jsx
 * @Description  :
 *
 * Copyright (c) 2024 by ${git_name_email}, All Rights Reserved.
 */
import PropTypes from 'prop-types'
import React, { memo } from 'react'
import { HeaderWrapper } from './style'
const SectionHeader = memo((props) => {
  const { title, subtitle = '' } = props
  return (
    <HeaderWrapper>
      <h2 className="title">{title}</h2>
      {
        subtitle && <div className='subtitle'>{subtitle}</div>
      }
    </HeaderWrapper>
  )
})

SectionHeader.propTypes = {
  title: PropTypes.string,
  subtitle: PropTypes.string,
}

export default SectionHeader
