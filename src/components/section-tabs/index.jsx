/*
 * @Author       : Leo chaodiangen@163.com
 * @Date         : 2024-04-22 17:25:29
 * @LastEditors  : Leo chaodiangen@163.com
 * @LastEditTime : 2024-04-22 17:44:36
 * @FilePath     : \react-demo\cdg_airbnb\src\components\section-tabs\index.jsx
 * @Description  :
 *
 * Copyright (c) 2024 by ${git_name_email}, All Rights Reserved.
 */
import PropTypes from 'prop-types'
import React, { memo, useState } from 'react'
import classNames from 'classnames'

import { TabsWrapper } from './style'
import ScrollView from '@/base-ui/scroll-view'

const SectionTabs = memo((props) => {
  const { tabNames = [], tabClick } = props
  const [currentIndex, setCurrentIndex] = useState(0)

  function itemClickHandle (index, name) {
    setCurrentIndex(index)
    tabClick(index, name)
  }
  return (
    <TabsWrapper>
      <ScrollView>
        {tabNames.map((item, index) => {
          return (
            <div
              className={classNames('item', { active: index === currentIndex })}
              onClick={(e) => itemClickHandle(index, item)}
              key={item}
            >
              {item}
            </div>
          )
        })}
      </ScrollView>
    </TabsWrapper>
  )
})

SectionTabs.propTypes = {
  tabNames: PropTypes.array,
}

export default SectionTabs
