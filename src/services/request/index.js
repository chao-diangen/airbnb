/**
 * @Author       : Leo chaodiangen@163.com
 * @Date         : 2024-04-12 13:53:23
 * @LastEditors  : Leo chaodiangen@163.com
 * @LastEditTime : 2024-04-26 10:24:13
 * @FilePath     : \react-demo\cdg_airbnb\src\services\request\index.js
 * @Description  :
 * @
 * @Copyright (c) 2024 by ${git_name_email}, All Rights Reserved.
 */

import axios from 'axios'
import { BASE_URL, TIMEOUT } from './config'
// 创建实例
class hRequest {
  constructor(baseURL, timeout) {
    this.instance = axios.create({
      baseURL,
      timeout,
    })
    this.instance.interceptors.response.use(
      (res) => {
        return res.data
      },
      (err) => {
        return err
      }
    )
  }

  request(config) {
    return this.instance.request(config)
  }

  get(config) {
    return this.request({ ...config, method: 'get' })
  }

  post(config) {
    return this.request({ ...config, method: 'post' })
  }
}

// eslint-disable-next-line import/no-anonymous-default-export
export default new hRequest(BASE_URL, TIMEOUT);
