import hRequest from "..";

export function getEntireRoomList (offset = 0, size=20) {
  return hRequest.get({
    url: 'entire/list',
    params: {
      offset,
      size
    }
  })
}