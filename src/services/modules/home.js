/**
 * @Author       : Leo chaodiangen@163.com
 * @Date         : 2024-04-19 14:46:18
 * @LastEditors  : Leo chaodiangen@163.com
 * @LastEditTime : 2024-04-24 11:19:04
 * @FilePath     : \react-demo\cdg_airbnb\src\services\modules\home.js
 * @Description  : 
 * @
 * @Copyright (c) 2024 by ${git_name_email}, All Rights Reserved. 
*/
import hRequest from '..'

export function getHomeGoodPriceData () {
  return hRequest.get({
    url:'/home/goodprice'
  })
}

export function getHomeHighScoreData () {
  return hRequest.get({
    url:'/home/highscore'
  })
}

export function getHomeDiscountData () {
  return hRequest.get({
    url:'/home/discount'
  })
}

export function getHomeHotRecommendData () {
  return hRequest.get({
    url:'/home/hotrecommenddest'
  })
}

export function getHomeLongForData () {
  return hRequest.get({
    url:'/home/longfor'
  })
}

export function getHomePlusData () {
  return hRequest.get({
    url:'/home/plus'
  })
}