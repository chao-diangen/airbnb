/**
 * @Author       : Leo chaodiangen@163.com
 * @Date         : 2024-04-12 13:50:49
 * @LastEditors  : Leo chaodiangen@163.com
 * @LastEditTime : 2024-04-19 14:48:39
 * @FilePath     : \react-demo\cdg_airbnb\src\services\index.js
 * @Description  : 
 * @
 * @Copyright (c) 2024 by ${git_name_email}, All Rights Reserved. 
*/
import hRequest from './request';

export default hRequest

export * from './modules/home'