/*
 * @Author       : Leo chaodiangen@163.com
 * @Date         : 2024-04-25 16:12:21
 * @LastEditors  : Leo chaodiangen@163.com
 * @LastEditTime : 2024-04-25 17:49:30
 * @FilePath     : \react-demo\cdg_airbnb\src\base-ui\indicator\index.jsx
 * @Description  :
 *
 * Copyright (c) 2024 by ${git_name_email}, All Rights Reserved.
 */

import PropTypes from 'prop-types'
import React, { memo, useEffect, useRef } from 'react'
import { IndicatorWrapper } from './style'

const Indicator = memo((props) => {
  const { selectIndex } = props
  const contentRef = useRef()
  useEffect(() => {
    // 获取selectIndex对应元素
    const selectItemEl = contentRef.current.children[selectIndex]
    const itemLeft = selectItemEl.offsetLeft
    const itemWidth = selectItemEl.clientWidth
    // content 宽度
    const contWidth = contentRef.current.clientWidth
    const contScroll = contentRef.current.scrollWidth

    // 获取要滚动的距离
    let distance = itemLeft + itemWidth * 0.5 - contWidth * 0.5
    if (distance < 0) distance = 0 // 左右情况处理
    // 总共距离
    const totalDistance = contScroll - contWidth
    if (distance > totalDistance) distance = totalDistance //右边特殊情况处理
    // 改变位置
    contentRef.current.style.transform = `translate(-${distance}px)`
  }, [selectIndex])
  return (
    <IndicatorWrapper>
      <div className="i-content" ref={contentRef}>
        {props.children}
      </div>
    </IndicatorWrapper>
  )
})

Indicator.propTypes = {
  selectIndex: PropTypes.number,
}

export default Indicator
