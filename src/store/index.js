/**
 * @Author       : Leo chaodiangen@163.com
 * @Date         : 2024-04-12 11:21:31
 * @LastEditors  : Leo chaodiangen@163.com
 * @LastEditTime : 2024-04-12 11:39:31
 * @FilePath     : \react-demo\cdg_airbnb\src\store\index.js
 * @Description  :
 * @
 * @Copyright (c) 2024 by ${git_name_email}, All Rights Reserved.
 */

import { configureStore } from '@reduxjs/toolkit'
import homeReducer from './modules/home'
import entireReducer from './modules/entire'
import detailReducer from './modules/detail'
import mainReducer from './modules/main'

// 两种方式配置 reducer
const store = configureStore({
  reducer: {
    home: homeReducer,
    entire: entireReducer,
    detail: detailReducer,
    main:mainReducer
  },
})

export default store
