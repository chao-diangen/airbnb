/**
 * @Author       : Leo chaodiangen@163.com
 * @Date         : 2024-04-12 11:28:27
 * @LastEditors  : Leo chaodiangen@163.com
 * @LastEditTime : 2024-04-12 11:28:34
 * @FilePath     : \react-demo\cdg_airbnb\src\store\modules\entire\index.js
 * @Description  : 
 * @
 * @Copyright (c) 2024 by ${git_name_email}, All Rights Reserved. 
*/
import reducer from './reducer'


export default reducer