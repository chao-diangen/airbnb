/**
 * @Author       : Leo chaodiangen@163.com
 * @Date         : 2024-04-12 11:28:55
 * @LastEditors  : Leo chaodiangen@163.com
 * @LastEditTime : 2024-04-25 14:55:11
 * @FilePath     : \react-demo\cdg_airbnb\src\store\modules\entire\reducer.js
 * @Description  :
 * @
 * @Copyright (c) 2024 by ${git_name_email}, All Rights Reserved.
 */
import * as actionTypes from './constants'

const initialState = {
  currentPage: 0,
  roomList: [],
  totalCount: 0,
  isLoading: false,
}

function reducer(state = initialState, action) {
  switch (action.type) {
    case actionTypes.CHANGE_CURRENT_PAG:
      return { ...state, currentPage: action.currentPage }
    case actionTypes.CHANGE_ROOM_LIST:
      return { ...state, roomList: action.roomList }
    case actionTypes.CHANGE_TOTAL_COUNT:
      return { ...state, totalCount: action.totalCount }
    case actionTypes.CHANGE_IS_LOADING:
      return { ...state, isLoading: action.isLoading }
    default:
      return state
  }
}

export default reducer
