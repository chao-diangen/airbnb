/**
 * @Author       : Leo chaodiangen@163.com
 * @Date         : 2024-04-12 11:28:38
 * @LastEditors  : Leo chaodiangen@163.com
 * @LastEditTime : 2024-04-25 14:56:10
 * @FilePath     : \react-demo\cdg_airbnb\src\store\modules\entire\actionCreators.js
 * @Description  :
 * @
 * @Copyright (c) 2024 by ${git_name_email}, All Rights Reserved.
 */

import { getEntireRoomList } from '@/services/modules/entire'
import * as actionTypes from './constants'

export const changeCurrentPageAction = (currentPage) => ({
  type: actionTypes.CHANGE_CURRENT_PAG,
  currentPage,
})

export const changeRoomListAction = (roomList) => ({
  type: actionTypes.CHANGE_ROOM_LIST,
  roomList,
})

export const changeTotalCountAction = (totalCount) => ({
  type: actionTypes.CHANGE_TOTAL_COUNT,
  totalCount,
})

export const changeIsLoadingAction = (isLoading) => ({
  type: actionTypes.CHANGE_IS_LOADING,
  isLoading,
})

export const fetchRoomListAction = (page = 0) => {
  return async (dispatch) => {
    // 0. 修改currentPage
    dispatch(changeCurrentPageAction(page))
    // 1. 获取当前页数
    dispatch(changeIsLoadingAction(true))
    const res = await getEntireRoomList(page * 20)
    const totalCount = res.totalCount
    const roomList = res.list
    dispatch(changeIsLoadingAction(false))
    // 2. 获取最新数据保存到redux中
    dispatch(changeRoomListAction(roomList))
    dispatch(changeTotalCountAction(totalCount))
  }
}
