/**
 * @Author       : Leo chaodiangen@163.com
 * @Date         : 2024-04-12 11:28:46
 * @LastEditors  : Leo chaodiangen@163.com
 * @LastEditTime : 2024-04-25 10:28:52
 * @FilePath     : \react-demo\cdg_airbnb\src\store\modules\entire\constants.js
 * @Description  : 
 * @
 * @Copyright (c) 2024 by ${git_name_email}, All Rights Reserved. 
*/

export const CHANGE_CURRENT_PAG = 'entire/change_current_pag'
export const CHANGE_ROOM_LIST = 'entire/change_room_list'
export const CHANGE_TOTAL_COUNT = 'entire/change_total_count'
export const CHANGE_IS_LOADING = 'entire/change_is_loading'
