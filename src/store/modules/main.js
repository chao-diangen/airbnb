/**
 * @Author       : Leo chaodiangen@163.com
 * @Date         : 2024-04-26 17:01:06
 * @LastEditors  : Leo chaodiangen@163.com
 * @LastEditTime : 2024-04-26 17:13:00
 * @FilePath     : \react-demo\cdg_airbnb\src\store\modules\main.js
 * @Description  :
 * @
 * @Copyright (c) 2024 by ${git_name_email}, All Rights Reserved.
 */
import { createSlice } from '@reduxjs/toolkit'

const mainSlice = createSlice({
  name: 'main',
  initialState: {
    headerConfig: {
      isFixed: false,
      topAlpha:false
    },
  },
  reducers: {
    changeHeaderConfigAction(state, { payload }) {
      state.headerConfig = payload
    },
  },
})
export const { changeHeaderConfigAction } = mainSlice.actions
export default mainSlice.reducer
