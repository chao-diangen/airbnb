/**
 * @Author       : Leo chaodiangen@163.com
 * @Date         : 2024-04-11 17:48:53
 * @LastEditors  : Leo chaodiangen@163.com
 * @LastEditTime : 2024-04-11 17:55:38
 * @FilePath     : \react-demo\cdg_airbnb\src\router\index.js
 * @Description  :
 * @
 * @Copyright (c) 2024 by ${git_name_email}, All Rights Reserved.
 */
import React from 'react'

import { Navigate } from "react-router-dom"

const Home = React.lazy(() => import("@/views/home"))
const Entire = React.lazy(() => import("@/views/entire"))
const Detail = React.lazy(() => import("@/views/detail"))
const Demo = React.lazy(() => import("@/views/demo"))

const routes = [
  {
    path: "/",
    element: <Navigate to="/home"/>
  },
  {
    path: "/home",
    element: <Home/>
  },
  {
    path: "/entire",
    element: <Entire/>
  },
  {
    path: "/detail",
    element: <Detail/>
  },
  {
    path: "/demo",
    element: <Demo/>
  }
]

export default routes
